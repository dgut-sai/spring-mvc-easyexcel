### 项目简介
本Demo演示如何使用Spring MVC创建Excel视图。
通过使用Spring MVC ContentNegotiation（内容协商），我们可以生成同一资源的多个视图。
Spring MVC 视图的功能就是构造HttpServletResponse，生成http请求的响应。

本Demo使用阿里巴巴开源的EasyExcel创建Excel视图，可以根据url的后缀自动选择生成excel文档类型（.xls或.xlsx格式）。
注意：生成大型excel文档时，使用流式xlsx视图是比较好选择。流视图使用较少的内存，可以提高大型excel文档的性能。

本Demo定义了一个新的视图 EasyExcelView 封装所有功能。设计上参考了函数式编程的思想。


参考：  
[https://memorynotfound.com/spring-mvc-excel-view-example/](https://memorynotfound.com/spring-mvc-excel-view-example/)  
[https://gitee.com/dgut-sai/spring-mvc-advanced-course](https://gitee.com/dgut-sai/spring-mvc-advanced-course)

### 欢迎交流
QQ: 2231068  
Email: lizhx@dgut.edu.cn
