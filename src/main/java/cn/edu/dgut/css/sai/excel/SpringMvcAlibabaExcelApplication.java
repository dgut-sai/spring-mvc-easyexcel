package cn.edu.dgut.css.sai.excel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMvcAlibabaExcelApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringMvcAlibabaExcelApplication.class, args);
    }

}
